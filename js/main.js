// ------------step-wizard-------------
$(document).ready(function () {
  $(".nav-tabs > li a[title]").tooltip();

  //Wizard
  $('a[data-toggle="tab"]').on("shown.bs.tab", function (e) {
    var target = $(e.target);

    if (target.parent().hasClass("disabled")) {
      return false;
    }
  });

  $(".next-step1").click(function (e) {
    var active = $(".wizard .nav-tabs li.active");
    active.next().removeClass("disabled");
    nextTab(active);
  });
  $(".prev-step").click(function (e) {
    var active = $(".wizard .nav-tabs li.active");
    prevTab(active);
  });

  // check condition next step 2

  $(".next-step2").click(function (e) {
    const active = $(".wizard .nav-tabs li.active");
    active.next().removeClass("disabled");
    let sub = 0;
    let flag = [];
    $(".ui-slider-handle b").map((item, index) => {
      const value = $(index).text().split("%")[0];
      sub = sub + +value;
    });
    if (sub < 100) {
      $("#step2 .actions .validation-advice").show();
      flag.push(false);
    } else {
      $("#step2 .actions .validation-advice").hide();
      flag.push(true);
    }
    $(".ui-slider-handle b").map((item, index) => {
      const value = $(index).text().split("%")[0];
      const valueSelect = $(index)
        .parents(".slide-bg")
        .find(".flavor-select")
        .val();
      if (valueSelect === "") {
        flag.push(false);
        $(index)
          .parents(".slide-bg")
          .find(".validation-advice")
          .addClass("d-block");
      } else {
        flag.push(true);
        $(index)
          .parents(".slide-bg")
          .find(".validation-advice")
          .removeClass("d-block");
      }
    });
    if (flag.indexOf(false) < 0) nextTab(active);
  });

  // disable option
  $(".slide-bg select").change(function (e) {
    $(this)
      .parents(".slide-bg")
      .find(".validation-advice")
      .removeClass("d-block");
    const valueOptionArr = [];
    $(".slide-bg select").map((index, option) => {
      if ($(option).val() !== "") return valueOptionArr.push($(option).val());
    });
    const options = $(this)
      .parents(".slide-bg")
      .siblings()
      .find(`select option`);
    options.map((index, option) => {
      const valueOption = $(option).val();
      if (valueOptionArr.indexOf(valueOption) >= 0) {
        $(option).attr("disabled", true);
      } else {
        $(option).removeAttr("disabled");
      }
    });
  });

  // change option step 1
  $(".size-item,.nicotine-item,.vg-item").click(function (e) {
    $(this).parent().find(".choose").removeClass("selected");
    $(this).find(".choose").addClass("selected");
    if ($(".size-item").find(".choose").hasClass("selected")) {
      $(".vg-level").addClass("can-choose");
    }
    if ($(".vg-item").find(".choose").hasClass("selected")) {
      $(".nicotine-level").addClass("can-choose");
    }
    if ($(".nicotine-level").find(".choose").hasClass("selected")) {
      $(".main-block .actions .next-step-btn").removeAttr("disabled");
    }
  });
  $(".choose-100").hide();
  $(".vg-item-50").click(function (e) {
    $(".choose-100").hide();
    $(".choose-50").show();
  });
  $(".vg-item-100").click(function (e) {
    $(".choose-50").hide();
    $(".choose-100").show();
  });

  $(".subscription-help-url").click(function (e) {
    $(this).parent().find(".subscription-product-popup").addClass("visible");
  });
  $(".subscription-message-button").click(function (e) {
    $(this)
      .parents(".product-subscriptions-info")
      .find(".subscription-product-popup")
      .removeClass("visible");
  });

  // plus and minus input when click
  $(".qty-plus-wrapper").click(function (e) {
    const value = +$(this).parents(".qty-wrapper").find(".qty-input").val();
    $(this)
      .parents(".qty-wrapper")
      .find(".qty-input")
      .val(value + 1);
  });
  $(".qty-minus-wrapper").click(function (e) {
    const value = +$(this).parents(".qty-wrapper").find(".qty-input").val();
    if (value > 1) {
      $(this)
        .parents(".qty-wrapper")
        .find(".qty-input")
        .val(value - 1);
    }
  });

  // slider ranger

  let slider1 = 0;
  let slider2 = 0;
  let slider3 = 0;
  let slider4 = 0;
  let slider5 = 0;

  $("#slider1").slider({
    min: 0,
    max: 100,
    step: 10,
    range: true,
    slide: function (event, ui) {
      slider1 = ui.value;
      const sub = slider1 + slider2 + slider3 + slider4 + slider5;
      $(".full").css("height", `${sub * 2.5}px`);
      if (sub >= 100) {
        $(".full").addClass("full-bottle");
      } else {
        $(".full").removeClass("full-bottle");
      }
      if (sub > 100) return false;

      $(ui.handle).html("<b>" + slider1 + "%" + "</b>");
    },
    start: function (event, ui) {
      slider1 = ui.value;
      $(ui.handle).html("<b>" + slider1 + "%" + "</b>");
    },
    stop: function (event, ui) {
      slider1 = ui.value;
      $(ui.handle).html("<b>" + slider1 + "%" + "</b>");
    },
  });

  $("#slider2").slider({
    min: 0,
    max: 100,
    step: 10,
    range: true,
    slide: function (event, ui) {
      slider2 = ui.value;
      const sub = slider1 + slider2 + slider3 + slider4 + slider5;
      $(".full").css("height", `${sub * 2.5}px`);
      if (sub >= 100) {
        $(".full").addClass("full-bottle");
      } else {
        $(".full").removeClass("full-bottle");
      }
      if (sub > 100) return false;
      $(ui.handle).html("<b>" + slider2 + "%" + "</b>");
    },
    start: function (event, ui) {
      slider2 = ui.value;
      $(ui.handle).html("<b>" + slider2 + "%" + "</b>");
    },
    stop: function (event, ui) {
      slider2 = ui.value;
      $(ui.handle).html("<b>" + slider2 + "%" + "</b>");
    },
  });

  $("#slider3").slider({
    min: 0,
    max: 100,
    step: 10,
    range: true,
    slide: function (event, ui) {
      slider3 = ui.value;
      const sub = slider1 + slider2 + slider3 + slider4 + slider5;
      $(".full").css("height", `${sub * 2.5}px`);
      if (sub >= 100) {
        $(".full").addClass("full-bottle");
      } else {
        $(".full").removeClass("full-bottle");
      }
      if (sub > 100) return false;
      $(ui.handle).html("<b>" + slider3 + "%" + "</b>");
    },
    start: function (event, ui) {
      slider3 = ui.value;
      $(ui.handle).html("<b>" + slider3 + "%" + "</b>");
    },
    stop: function (event, ui) {
      slider3 = ui.value;
      $(ui.handle).html("<b>" + slider3 + "%" + "</b>");
    },
  });

  $("#slider4").slider({
    min: 0,
    max: 100,
    step: 10,
    range: true,
    slide: function (event, ui) {
      slider4 = ui.value;
      const sub = slider1 + slider2 + slider3 + slider4 + slider5;
      $(".full").css("height", `${sub * 2.5}px`);
      if (sub >= 100) {
        $(".full").addClass("full-bottle");
      } else {
        $(".full").removeClass("full-bottle");
      }
      if (sub > 100) return false;
      $(ui.handle).html("<b>" + slider4 + "%" + "</b>");
    },
    start: function (event, ui) {
      slider4 = ui.value;
      $(ui.handle).html("<b>" + slider4 + "%" + "</b>");
    },
    stop: function (event, ui) {
      slider4 = ui.value;
      $(ui.handle).html("<b>" + slider4 + "%" + "</b>");
    },
  });

  $("#slider5").slider({
    min: 0,
    max: 100,
    step: 10,
    range: true,
    slide: function (event, ui) {
      slider5 = ui.value;
      const sub = slider1 + slider2 + slider3 + slider4 + slider5;
      $(".full").css("height", `${sub * 2.5}px`);
      if (sub >= 100) {
        $(".full").addClass("full-bottle");
      } else {
        $(".full").removeClass("full-bottle");
      }
      if (sub > 100) return false;
      $(ui.handle).html("<b>" + slider5 + "%" + "</b>");
    },
    start: function (event, ui) {
      slider5 = ui.value;
      $(ui.handle).html("<b>" + slider5 + "%" + "</b>");
    },
    stop: function (event, ui) {
      slider5 = ui.value;
      $(ui.handle).html("<b>" + slider5 + "%" + "</b>");
    },
  });

  function nextTab(elem) {
    $(elem).next().find('a[data-toggle="tab"]').click();
  }
  function prevTab(elem) {
    $(elem).prev().find('a[data-toggle="tab"]').click();
  }

  $(".nav-tabs").on("click", "li", function () {
    $(".nav-tabs li.active").removeClass("active");
    $(this).addClass("active");
  });
});
